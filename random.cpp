///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file random.cpp
/// @version 1.0
///
/// Test file for random attribute generator
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   01_APR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "node.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;

int main() {

/*   srand (time(NULL));

   cout << "Random Attribute Generator:" << endl;
   cout << "   Gender: " << Animal::getRandomGender() << endl;
   cout << "   Color: " << Animal::getRandomColor() << endl;
   cout << "   Bool: " << std::boolalpha << Animal::getRandomBool() << endl;
   cout << "   Weight: " << Animal::getRandomWeight(9.2, 20.5) << endl;
   cout << "   Name: " << Animal::getRandomName() << endl;
*/

   cout << "Hello World" << endl; 
   Node node;
   SingleLinkedList list;
   
   cout << "List is empty?: " << boolalpha << list.empty() << endl;
   
   list.push_front(new Node());

   cout << "Add node" << endl;
   cout << "List is empty?: " << boolalpha << list.empty() << endl;
   
   list.push_front(new Node());
   cout << "Add node" << endl;
   cout << "Size: " << list.size() << endl;
   
   list.pop_front();
   cout << "Remove node" << endl;
   cout << "Size: " << list.size() << endl;
   
   list.push_front(new Node());
   list.push_front(new Node());
   cout << "Add 2 more nodes" << endl;
   cout << "Size: " << list.size() << endl;  
   cout << "List is empty?: " << boolalpha << list.empty() << endl;

}
