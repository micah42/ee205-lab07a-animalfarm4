///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.hpp
/// @version 1.0
///
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   01_APR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

namespace animalfarm {

class Node {
   protected:
      Node* next = nullptr;

      friend class SingleLinkedList;
};

}//namespace animalfarm 

