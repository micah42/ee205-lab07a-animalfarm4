///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// SingleLinkedList class
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   01_APR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

using namespace std;

namespace animalfarm {

class SingleLinkedList {
protected:
    Node* head = nullptr;
    unsigned int count = 0;

public:
    const bool empty() const;
    void push_front(Node* newNode);
    Node* pop_front();
    Node* get_first() const;
    Node* get_next(const Node* currentNode) const;
    unsigned int size() const;
};

} // namespace animalfarm


